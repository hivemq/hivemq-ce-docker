FROM openjdk:12-ea-20-jdk-alpine

ARG HIVEMQ_CE_VERSION=2019.1
ARG HIVEMQ_GID=10000
ARG HIVEMQ_UID=10000

# Additional JVM options, may be overwritten by user
ENV JAVA_OPTS "-XX:+UseNUMA"

COPY config.xml /opt/config.xml

# HiveMQ setup
RUN curl -L https://github.com/hivemq/hivemq-community-edition/releases/download/${HIVEMQ_CE_VERSION}/hivemq-ce-${HIVEMQ_CE_VERSION}.zip -o /opt/hivemq-${HIVEMQ_CE_VERSION}.zip \
    && unzip /opt/hivemq-${HIVEMQ_CE_VERSION}.zip  -d /opt/\
    && rm -f /opt/hivemq-${HIVEMQ_CE_VERSION}.zip \
    && ln -s /opt/hivemq-ce-${HIVEMQ_CE_VERSION} /opt/hivemq \
    && mv /opt/config.xml /opt/hivemq-ce-${HIVEMQ_CE_VERSION}/conf/config.xml \
    && groupadd --gid ${HIVEMQ_GID} hivemq \
    && useradd -g hivemq -d /opt/hivemq -s /bin/bash --uid ${HIVEMQ_UID} hivemq \
    && chown -R hivemq:hivemq /opt/hivemq-ce-${HIVEMQ_CE_VERSION} \
    && chmod -R 777 /opt \
    && chmod +x /opt/hivemq/bin/run.sh

# Substitute eval for exec and replace OOM flag if necessary (for older releases). This is necessary for proper signal propagation
RUN sed -i -e 's|eval \\"java\\" "$HOME_OPT" "$JAVA_OPTS" -jar "$JAR_PATH"|exec "java" $HOME_OPT $JAVA_OPTS -jar "$JAR_PATH"|' /opt/hivemq/bin/run.sh && \
    sed -i -e "s|-XX:OnOutOfMemoryError='sleep 5; kill -9 %p'|-XX:+CrashOnOutOfMemoryError|" /opt/hivemq/bin/run.sh

# Make broker data persistent throughout stop/start cycles
VOLUME /opt/hivemq/data

# Persist log data
VOLUME /opt/hivemq/log

EXPOSE 1883

WORKDIR /opt/hivemq
USER ${HIVEMQ_UID}

CMD ["/opt/hivemq/bin/run.sh"]
